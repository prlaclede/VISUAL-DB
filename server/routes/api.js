const express = require('express');
const router = express.Router();
const path = require('path');
const sqlite3 = require('sqlite3').verbose();

String.prototype.format = function () {
    a = this;
    for (k in arguments) {
        a = a.replace("{" + k + "}", arguments[k])
    }
    return a
}

let knex = require("knex")({
    client: "sqlite3",
    connection: {
        filename: "./local_notes/space.db"
    },
    useNullAsDefault: true
});


// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};

// Get columns
router.get('/columns', (req, res) => {
    let notesResults = knex.raw("PRAGMA table_info(SPACE);");
    notesResults.then(function (notes) {
        res.send(notes);
    });
});

// Get notes
router.get('/notes', (req, res) => {
    let notesResults = knex.select("*").from("SPACE").where('STATUS', '=', 'open').orderBy('ID', 'DESC');
    notesResults.then(function (notes) {
        res.send(notes);
    });
});

// Get saved filters
router.get('/filters', (req, res) => {
    let filterResults = knex.select("*").from("FILTERS");
    filterResults.then(function (filters) {
        res.send(filters);
    });
});

router.post('/filterNotes', (req, res) => {
    let filter = req.body;
    let hasWhere = false; //boolean to track if we've started a where statement
    let notesSelect = knex.select("*").from("SPACE");

    let noteFilterQuery = filter['query'];

    /* if we have all the parameters required for column level filtering */
    if (filter['column'] && filter['operator'] && filter['query']) {
        if (filter['type'] === 'DATE') {
            noteFilterQuery = new Date(filter['query']).getTime();
        }

        if (filter['operator'] === 'like') {
            noteFilterQuery = "%" + noteFilterQuery + "%";
        }

        notesSelect = notesSelect
            .where(filter['column'], filter['operator'], noteFilterQuery);

        hasWhere = true;
    }

    if (filter['today']) {
        var today = new Date();
        var yesterday = new Date().setDate(today.getDate() - 1);
        var tomorrow = new Date().setDate(today.getDate() + 1);
        if (hasWhere) {
            notesSelect = notesSelect.andWhere('DATE', '>', yesterday).andWhere('DATE', '<', tomorrow);
        } else {
            notesSelect = notesSelect.where('DATE', '>', yesterday).andWhere('DATE', '<', tomorrow);
        }
    }

    if (filter['orderBy'] && filter['order']) {
        notesSelect = notesSelect
            .orderBy(filter['orderBy'], filter['order']);
    } else {
        notesSelect = notesSelect.orderBy('ID', 'DESC');
    }

    if (hasWhere) {
        if (filter['archives'] && filter['open']) {
            notesSelect = notesSelect.andWhere('STATUS', '=', 'archived').orWhere('STATUS', '=', 'open');
        } else if (filter['archives'] && !filter['open']) {
            notesSelect = notesSelect.andWhere('STATUS', '=', 'archived');
        } else if (!filter['archives'] && filter['open']) {
            notesSelect = notesSelect.andWhere('STATUS', '=', 'open');
        } else {
            notesSelect = notesSelect.andWhere('STATUS', '=', 'open')
        }

    } else {
        if (filter['archives'] && filter['open']) {
            notesSelect = notesSelect.where('STATUS', '=', 'archived').orWhere('STATUS', '=', 'open');
        } else if (filter['archives'] && !filter['open']) {
            notesSelect = notesSelect.where('STATUS', '=', 'archived');
        } else if (!filter['archives'] && filter['open']) {
            notesSelect = notesSelect.where('STATUS', '=', 'open');
        } else {
            notesSelect = notesSelect.where('STATUS', '=', 'open')
        }
    }


    notesSelect.then(function (filteredNotes) {
        res.send(filteredNotes);
    });
});

router.post('/note/save', function (req, res) {
    let note = req.body;
    let noteSave = knex("SPACE")
        .insert({
            DATE: new Date(note.DATE).getTime(),
            TITLE: note.TITLE,
            NOTE: note.NOTE,
            PARENT: note.PARENT
        });
    noteSave.then(function (noteId) {
        res.send(noteId);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500)
    });
});

router.post('/note/update', function (req, res) {
    let note = req.body;
    let noteUpdate = knex("SPACE").where("ID", "=", note.ID)
        .update({
            DATE: new Date(note.DATE).getTime(),
            TITLE: note.TITLE,
            NOTE: note.NOTE,
            PARENT: note.PARENT
        });

    noteUpdate.then(function () {
        res.sendStatus(200);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500)
    });
});

router.post('/note/archive', function (req, res) {
    let note = req.body;

    let noteArchive = knex("SPACE").where("ID", "=", note.ID)
        .update({
            STATUS: 'archived'
        });

    noteArchive.then(function () {
        res.sendStatus(200);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500)
    });
});

router.post('/filter/save', function (req, res) {
    let filter = req.body;
    let filterSave = knex("FILTERS")
        .insert({
            NAME: filter.name,
            COLUMN: filter.column,
            OPERATOR: filter.operator,
            QUERY: filter.query,
            DEFAULT: filter.defaultFilter,
            ARCHIVES: filter.archives,
            OPEN: filter.open,
            ORDERBY: filter.orderby,
            ORDER: filter.order,
            TODAY: filter.today
        });
    filterSave.then(function (filterId) {
        res.send(filterId);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500)
    });
});

router.post('/filter/delete', function (req, res) {
    let filter = req.body;
    let filterDelete = knex("FILTERS").where("ID", "=", filter.ID).del();

    filterDelete.then(function () {
        res.sendStatus(200);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500)
    });
});

module.exports = router;