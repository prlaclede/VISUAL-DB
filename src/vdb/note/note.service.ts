import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

import { SpaceService } from '../space/space.service';
import { CommonService } from '../common/common.service';

@Injectable()
export class NoteService {

    constructor(private _http: Http, private _cs: CommonService, private _ss: SpaceService) { }

    private cache = this._ss.data;

    initNoteForms() {
        this.cache['newNoteIndex'] = 1;

        this.cache['notesFormGroup'] = new FormGroup({});

        _.each(this.cache['notes'], (note) => {
            this.cache['noteProperties']['notes'][note.ID] = new Array();
            this.cache['noteProperties']['notes'][note.ID]['saving'] = false;
            this.cache['notesFormGroup'].controls[note.ID] = new FormGroup({
                ID: new FormControl(note.ID),
                DATE: new FormControl(this._cs.getDate(note.DATE)),
                TITLE: new FormControl(note.TITLE),
                NOTE: new FormControl(note.NOTE),
                PARENT: new FormControl(note.PARENT)
            });
        });

        // let notesTree = this.makeNotesTree();
    }

    // makeNotesTree() {
    //     var groupedNotes = _.groupBy(this.notes, 'PARENT');
    //     _.each(_.omit(groupedNotes, ['', 'null']), (children, parentId) => {
    //         let parentNoteFormGroup = this.notesFormGroup.controls[parentId]['controls'];
    //         parentNoteFormGroup.children = new FormGroup({});

    //         _.each(children, (child) => {
    //             parentNoteFormGroup.children.controls[child.ID] = this.notesFormGroup.controls[child.ID];
    //             this.notesFormGroup.removeControl(child.ID);
    //         });
    //     });

    //     console.log(this.notesFormGroup);
    //     return groupedNotes;
    // }

    getNotesArray(controlGroup) {
        return Object.keys(controlGroup.controls);
    }

    addNote() {
        this.cache['noteProperties']['newNotes'][this.cache['newNoteIndex']] = new Array();
        this.cache['noteProperties']['newNotes'][this.cache['newNoteIndex']]['saving'] = false;
        this.cache['newNotesFormGroup'].controls[this.cache['newNoteIndex']] = new FormGroup({
            ID: new FormControl(this.cache['newNoteIndex']),
            DATE: new FormControl(new Date()),
            TITLE: new FormControl(),
            NOTE: new FormControl(),
            PARENT: new FormControl()
        });

        this.cache['newNoteIndex'] += 1;
    }

    saveNote(noteFG) {
        let newNoteValue = noteFG.value;
        this.cache['noteProperties']['newNotes'][newNoteValue['ID']]['saving'] = true;
        this._ss.saveNote(newNoteValue)
            .subscribe(res => {
                if (res.status === 200) {
                    let noteId = JSON.parse(res['_body'])[0];
                    this.cache['noteProperties']['newNotes'][newNoteValue['ID']]['saving'] = false;
                    this.deleteNewNote(noteFG);
                    /* add to the notes FormGroup */
                    this.cache['notesFormGroup'].controls[noteId] = new FormGroup({
                        ID: new FormControl(noteId),
                        DATE: new FormControl(this._cs.getDate(newNoteValue.DATE)),
                        TITLE: new FormControl(newNoteValue.TITLE),
                        NOTE: new FormControl(newNoteValue.NOTE),
                        PARENT: new FormControl(newNoteValue.PARENT)
                    });
                    /* add to the noteProperties array */
                    this.cache['noteProperties']['notes'][noteId] = new Array();
                    this.cache['noteProperties']['notes'][noteId]['saving'] = false;
                    /* add to the notes array */
                    let newNoteObj = {
                        ID: noteId,
                        DATE: newNoteValue.DATE,
                        TITLE: newNoteValue.TITLE,
                        NOTE: newNoteValue.NOTE,
                        PARENT: newNoteValue.PARENT
                    }

                    // if (ASC) {
                    this.cache['notes'].unshift(newNoteObj);
                    // } else { DESC
                    // this.notes.unshift(newNoteObj);
                    // }
                } else {
                    this.cache['noteProperties']['newNotes'][newNoteValue['ID']]['saving'] = false;
                }
            });
    }

    updateNote(noteFG) {
        let noteId = noteFG.controls.ID.value;
        this.cache['noteProperties']['notes'][noteId]['saving'] = true;
        this._ss.updateNote(noteFG.value).subscribe(res => {
            if (res.status === 200) {
                this.cache['noteProperties']['notes'][noteId]['saving'] = false;
            } else {
                this.cache['noteProperties']['notes'][noteId]['saving'] = false;
            }
        });
    }

    deleteNewNote(noteFG) {
        let noteId = noteFG.controls.ID.value;
        this.cache['newNotesFormGroup'].removeControl(noteId);
        this.cache['newNoteIndex'] -= 1;
    }

    deleteSavedNote(noteFG) {
        let noteId = noteFG.controls.ID.value;
        let noteIndex = _.findKey(this.cache['notes'], { ID: noteId });
        this.cache['notes'].splice(noteIndex, 1);
        this.cache['notesFormGroup'].removeControl(noteId);
        this.cache['noteProperties']['notes'].splice(noteId, 1);
        this._ss.archiveNote(noteFG.value).subscribe(res => {
            if (res.status === 200) {
                console.log('note deleted');
            } else {
                console.log(res);
            }
        });
    }

}
